FROM python:3.6-jessie

WORKDIR /opt
ADD / /opt
RUN pip install -r requierements.txt
RUN pip install requests
RUN pip install bs4
ENTRYPOINT ["python", "-u", "/opt/main.py"]
